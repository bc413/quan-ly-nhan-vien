// hop lệ thì return true

let lisThongBao = ["Vui lòng nhập tài khoản", "Vui lòng nhập tên","Vui lòng nhập Email", "Vui lòng nhập Mật khẩu", "Vui lòng nhập ngày", "Vui lòng nhập lương", "Vui lòng nhập giờ làm"]

function kiemTraNhap(idInput, idThongBao,index){
    let valueInput = document.getElementById(idInput).value;
    let thongBao = document.getElementById(idThongBao)
    if(valueInput == ""){
        thongBao.innerHTML = lisThongBao[index]
        thongBao.style.display = "block";
        return false;
    }else{
        thongBao.style.display = "none";
        return true;
    }
}

function kiemTraChucVu(idSelec, idThongBao){
    let selec = document.getElementById(idSelec);
    let thongBao = document.getElementById(idThongBao)
    if(selec.selectedIndex == 0){
        thongBao.innerHTML = "Vui lòng chọn chức vụ"
        thongBao.style.display = "block";
        return false;
    }else{
        thongBao.style.display = "none";
        return true;
    }
}


function kiemTraKySo(idInput, idThongBao){
    let valueInput = document.getElementById(idInput).value;
    let thongBao = document.getElementById(idThongBao);
    let kyTu = /^[0-9]+$/;
    if(valueInput.match(kyTu)){
        thongBao.style.display = "none";
        return true;
    }else{
        thongBao.style.display = "Block";
        thongBao.innerHTML = "Chỉ được nhập số"
        return false;
    }
}

function ktChieuDai(idName,idInput, idThongBao,minLength,maxLength){
    let valueInput = document.getElementById(idInput).value;
    let thongBao = document.getElementById(idThongBao);
    if(valueInput.length<minLength || valueInput.length>maxLength){
        thongBao.innerHTML = `${idName} tối đa ${minLength}  -  ${maxLength} kí tự`
        thongBao.style.display = "Block";
        return false;
    }else{
        thongBao.style.display = "none";
        return true;
    }
}


function kiemTraTen(idInput, idThongBao){
    let valueInput = document.getElementById(idInput).value;
    let thongBao = document.getElementById(idThongBao);
    let kyTu = new RegExp("^[A-Za-z]+$");
    if(kyTu.test(valueInput)){
        thongBao.style.display = "none";
        return true;
    }else{
        thongBao.innerHTML = "Chỉ được nhập chữ cái(viết liền không dấu)"
        thongBao.style.display = "block";
        return false;
    }
}


function kiemTraEmail(idInput, idThongBao){
    let valueInput = document.getElementById(idInput).value;
    let thongBao = document.getElementById(idThongBao);
    let kyTu = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(valueInput.match(kyTu)){
        thongBao.style.display = "none";
        return true;
    }else{
        thongBao.style.display = "Block";
        thongBao.innerHTML = "Vui lòng đúng email"
        return false;
    }
}

function kiemTraPass(idInput, idThongBao){
    let valueInput = document.getElementById(idInput).value;
    let thongBao = document.getElementById(idThongBao);
    let kyTu = new RegExp("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-])");
    if(kyTu.test(valueInput)){
        thongBao.style.display = "none";
        return true;
    }else{
        thongBao.innerHTML = "Mật khẩu phải có ít nhất 1 số, 1 chữ in hoa, 1 ký tự đặc biệt "
        thongBao.style.display = "block";
        return false;
    }
}


function ktLuong(idInput, idThongBao){
    let valueInput = document.getElementById(idInput).value*1;
    let thongBao = document.getElementById(idThongBao);
    if(valueInput< 1e+6 || valueInput> 2e+7 ){
        thongBao.innerHTML = "Lương cơ bản phải lớn 1.000.000, và bé hơn 20.000.000";
        thongBao.style.display = "block";
        return false;
    }else{
        thongBao.style.display = "none";
        return true;
    }
}

function soGioLam(idInput, idThongBao){
    let valueInput = document.getElementById(idInput).value*1;
    let thongBao = document.getElementById(idThongBao);
    if(valueInput< 80 || valueInput> 200 ){
        thongBao.innerHTML = "Giờ làm trong tháng 80 - 200 giờ";
        thongBao.style.display = "block";
        return false;
    }else{
        thongBao.style.display = "none";
        return true;
    }
}


function timViTri(idnv,arrDS){
    let viTri = -1;
    for(let i=0;i <arrDS.length; i++){
        if(arrDS[i].taiKhoan == idnv){
            viTri = i;
        }
    }
    return viTri;
}

function kiemTraTRung(idTaiKhoan,arrDS,idThongBao){
    let maNX = document.querySelector(idTaiKhoan).value;
    let thongBao = document.getElementById(idThongBao);
    let viTri = arrDS.findIndex(function(item){
        let checkIndex = item.taiKhoan == maNX
        return checkIndex;
    })
    if(viTri != -1){
        thongBao.innerHTML = "Tai khoan bi trung";
        thongBao.style.display = "block";
        return false;
    }else{
        thongBao.style.display = "none";
        return true;
    }
}